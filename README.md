# dmenu-saltmine

My build of Suckless dmenu.

Source:

- [https://tools.suckless.org/dmenu/](https://tools.suckless.org/dmenu/)
- [https://dl.suckless.org/tools/dmenu-5.0.tar.gz](https://dl.suckless.org/tools/dmenu-5.0.tar.gz)

Patches applied:

- line height [https://tools.suckless.org/dmenu/patches/line-height/](https://tools.suckless.org/dmenu/patches/line-height/)
- numbers [https://tools.suckless.org/dmenu/patches/numbers/](https://tools.suckless.org/dmenu/patches/numbers/)
